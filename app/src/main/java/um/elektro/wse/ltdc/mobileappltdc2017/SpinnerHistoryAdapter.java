package um.elektro.wse.ltdc.mobileappltdc2017;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DERI on 06/09/2017.
 */

public class SpinnerHistoryAdapter extends ArrayAdapter {

    private ArrayList<SpinnerHistoryModel> models;

    public SpinnerHistoryAdapter(@NonNull Context context, @LayoutRes int resource, @IdRes int textViewResourceId, @NonNull List objects) {
        super(context, resource, textViewResourceId, objects);
        models = (ArrayList<SpinnerHistoryModel>) objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.spinner_item, parent, false);
        SpinnerHistoryModel model = models.get(position);
        TextView name = (TextView) view.findViewById(R.id.history_team_name);
        TextView stop = (TextView) view.findViewById(R.id.history_stop_time);

        int bg = Color.parseColor(model.getBackground()),
                text = Color.parseColor(model.getTextColor());

        name.setBackgroundColor(bg); name.setTextColor(text);
        stop.setBackgroundColor(bg); stop.setTextColor(text);
        name.setText(model.getTeamName()); stop.setText(model.getStopTime());

        return view;
    }
}
