package um.elektro.wse.ltdc.mobileappltdc2017;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.*;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.client;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.getDeviation;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.getDrawableViewTeamA;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.getKategori;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.getTeam;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.reader;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.writer;

public class MainActivity extends AppCompatActivity {

    static final int RC_CONFIG = 991;

    // View variable
    Button kirim;
    ImageView logo, menit1Up, menit2Up, detik1Up, detik2Up,
            milis1Up, milis2Up, milis3Up, menit1Down, menit2Down,
            detik1Down, detik2Down, milis1Down, milis2Down, milis3Down;
    TextView minJarakA, minJarakB, plusJarakA, plusJarakB,
            minFoulA, minFoulB, plusFoulA, plusFoulB,
            timerMulai, timerBerhenti, timerMode, namaA, namaB,
            berhentiA, berhentiB, sisaA, sisaB, statusKoneksi,
            menit1, menit2, detik1, detik2, milis1, milis2, milis3;
    ImageButton stopA, stopB, refreshBtn;
    EditText jarakA, jarakB, foulA, foulB;
    Spinner historySpinner;
    AlertDialog logout, info, refresh, timePicker;
    View ubahSisaA, ubahSisaB;

    // Handle server command variable
    Handler clientHandler, loginHandler;
    ClientThread clientThread;

    // Other variable
    Intent configIntent;
    ArrayList<String> timePickerHistoryData;
    ArrayAdapter<String> timePickerHistoryAdapter;
    int durasi;
    static boolean running;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Intent
        configIntent = new Intent(this, ConfigActivity.class);
        durasi = 0;

        prepareUtils();
        prepareView();
        prepareEventClick();

        if (client == null || writer == null) {
            startActivityForResult(configIntent, RC_CONFIG);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_CONFIG) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Konfigurasi sukses !", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Konfigurasi dibatalkan !", Toast.LENGTH_SHORT).show();
            }

            configMobile();

            if (clientThread != null) clientThread.kill();
            clientThread = new ClientThread(clientHandler);
            clientThread.start();
        } else super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume(); configMobile();
    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
    }

    private void prepareUtils() {
        // Handler
        clientHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        String status = (msg.arg2 > 0?"terhubung":"offline");
                        statusKoneksi.setText(status);
                        if (msg.obj != null) {
                            Toast.makeText(MainActivity.this, msg.obj.toString(),
                                    msg.arg1).show();
                        }
                        if (msg.arg2 < 1) logout.show();
                        break;
                    case 2:
                        processCommand(msg.obj.toString());
                        break;
                    default:
                        Toast.makeText(MainActivity.this, "Tidak dapat memproses " +
                                "pesan handler: "+msg.what, Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        // Login Handler
        loginHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        Toast.makeText(MainActivity.this, msg.obj.toString()+" ke server!",
                                Toast.LENGTH_SHORT).show();
                        statusKoneksi.setText(msg.obj.toString());
                        if (msg.arg1 == 1) {
                            clientThread = new ClientThread(clientHandler);
                            clientThread.start();
                        }
                        break;
                    default:
                        Toast.makeText(MainActivity.this, "Tidak dapat memproses " +
                                        "pesan handler: "+msg.what+" - "+msg.obj.toString(),
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        // AlertDialog
        logout = new AlertDialog.Builder(MainActivity.this).create();
        logout.setTitle("Alert");
        logout.setMessage("Kembali ke Menu Awal? Koneksi akan Terputus.");
        logout.setCancelable(false);
        logout.setButton(AlertDialog.BUTTON_POSITIVE, "OKE",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (clientThread != null) clientThread.kill();
                        startActivityForResult(configIntent, RC_CONFIG);
                    }
                });
        logout.setButton(AlertDialog.BUTTON_NEUTRAL, "KONEKSI ULANG",
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (clientThread != null) clientThread.kill();
                new LoginThread(loginHandler, ipUtil, portUtil, getKategori(), getTeam()).start();
            }
        });
        logout.setButton(AlertDialog.BUTTON_NEGATIVE, "BATAL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        refresh = new AlertDialog.Builder(MainActivity.this).create();
        refresh.setTitle("Alert");
        refresh.setMessage("Bersihkan tampilan dan request data baru ke Operator ?");
        refresh.setCancelable(true);
        refresh.setButton(AlertDialog.BUTTON_NEUTRAL, "SEMUA",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        refreshTeam();
                        sendMessages("Meminta data team & timer baru.", "Operator");
                    }
                });
        refresh.setButton(AlertDialog.BUTTON_POSITIVE, "DATA TEAM",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sendMessages("Meminta data team baru.", "Operator");
                    }
                });
        refresh.setButton(AlertDialog.BUTTON_NEGATIVE, "DATA TIMER",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sendMessages("Meminta data timer baru.", "Operator");
                    }
                });

        info = new AlertDialog.Builder(MainActivity.this).create();
        info.setTitle("Info");
        info.setCancelable(true);
        info.setButton(AlertDialog.BUTTON_NEUTRAL, "OKE",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        View timeView = getLayoutInflater().inflate(R.layout.time_picker_dialog, null);
        timePicker = new AlertDialog.Builder(MainActivity.this).create();
        timePicker.requestWindowFeature(Window.FEATURE_NO_TITLE);
        timePicker.setCancelable(true); timePicker.setView(timeView);

        // Timepicker view child
        timePickerHistoryData = new ArrayList<>();
        timePickerHistoryAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, timePickerHistoryData);
        timePickerHistoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        historySpinner = (Spinner) timeView.findViewById(R.id.history_spinner);
        historySpinner.setAdapter(timePickerHistoryAdapter);

        menit1 = (TextView) timeView.findViewById(R.id.menit1);
        menit2 = (TextView) timeView.findViewById(R.id.menit2);
        detik1 = (TextView) timeView.findViewById(R.id.detik1);
        detik2 = (TextView) timeView.findViewById(R.id.detik2);
        milis1 = (TextView) timeView.findViewById(R.id.milis1);
        milis2 = (TextView) timeView.findViewById(R.id.milis2);
        milis3 = (TextView) timeView.findViewById(R.id.milis3);
        menit1Up = (ImageView) timeView.findViewById(R.id.menit1_up);
        menit2Up = (ImageView) timeView.findViewById(R.id.menit2_up);
        detik1Up = (ImageView) timeView.findViewById(R.id.detik1_up);
        detik2Up = (ImageView) timeView.findViewById(R.id.detik2_up);
        milis1Up = (ImageView) timeView.findViewById(R.id.milis1_up);
        milis2Up = (ImageView) timeView.findViewById(R.id.milis2_up);
        milis3Up = (ImageView) timeView.findViewById(R.id.milis3_up);
        menit1Down = (ImageView) timeView.findViewById(R.id.menit1_down);
        menit2Down = (ImageView) timeView.findViewById(R.id.menit2_down);
        detik1Down = (ImageView) timeView.findViewById(R.id.detik1_down);
        detik2Down = (ImageView) timeView.findViewById(R.id.detik2_down);
        milis1Down = (ImageView) timeView.findViewById(R.id.milis1_down);
        milis2Down = (ImageView) timeView.findViewById(R.id.milis2_down);
        milis3Down = (ImageView) timeView.findViewById(R.id.milis3_down);
    }

    private void prepareView() {

        // Text View
        minJarakA = (TextView) findViewById(R.id.teamA_jarak_min);
        minJarakB = (TextView) findViewById(R.id.teamB_jarak_min);
        plusJarakA = (TextView) findViewById(R.id.teamA_jarak_plus);
        plusJarakB = (TextView) findViewById(R.id.teamB_jarak_plus);
        minFoulA = (TextView) findViewById(R.id.teamA_foul_min);
        minFoulB = (TextView) findViewById(R.id.teamB_foul_min);
        plusFoulA = (TextView) findViewById(R.id.teamA_foul_plus);
        plusFoulB = (TextView) findViewById(R.id.teamB_foul_plus);
        timerMulai = (TextView) findViewById(R.id.timer_mulai);
        timerBerhenti = (TextView) findViewById(R.id.timer_berhenti);
        timerMode = (TextView) findViewById(R.id.timer_mode);
        namaA = (TextView) findViewById(R.id.teamA_nama);
        namaB = (TextView) findViewById(R.id.teamB_nama);
        berhentiA = (TextView) findViewById(R.id.teamA_berhenti);
        berhentiB = (TextView) findViewById(R.id.teamB_berhenti);
        sisaA = (TextView) findViewById(R.id.teamA_timer);
        sisaB = (TextView) findViewById(R.id.teamB_timer);
        statusKoneksi = (TextView) findViewById(R.id.status_koneksi);

        // ImageView
        logo = (ImageView) findViewById(R.id.logo_popo);

        // Button
        kirim = (Button) findViewById(R.id.kirim);

        // ImageButton
        stopA = (ImageButton) findViewById(R.id.teamA_stop);
        stopB = (ImageButton) findViewById(R.id.teamB_stop);
        refreshBtn = (ImageButton) findViewById(R.id.refresh);

        // EditText
        jarakA = (EditText) findViewById(R.id.teamA_jarak);
        jarakB = (EditText) findViewById(R.id.teamB_jarak);
        foulA = (EditText) findViewById(R.id.teamA_foul);
        foulB = (EditText) findViewById(R.id.teamB_foul);

        // View
        ubahSisaA = findViewById(R.id.teamA_ubah_timer);
        ubahSisaB = findViewById(R.id.teamB_ubah_timer);
    }

    private void prepareEventClick() {
        // Jarak Team A
        minJarakA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int jarak;
                try {
                    jarak = Integer.parseInt(jarakA.getText().toString());
                } catch (NumberFormatException ne) {
                    jarak = 0;
                }
                if (--jarak < 0) jarak = 0;
                jarakA.setText(jarak+"");
            }
        });
        plusJarakA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int jarak;
                try {
                    jarak = Integer.parseInt(jarakA.getText().toString());
                } catch (NumberFormatException ne) {
                    jarak = 0;
                }
                jarak++;
                jarakA.setText(jarak+"");
            }
        });

        // Jarak Team B
        minJarakB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int jarak;
                try {
                    jarak = Integer.parseInt(jarakB.getText().toString());
                } catch (NumberFormatException ne) {
                    jarak = 0;
                }
                if (--jarak < 0) jarak = 0;
                jarakB.setText(jarak+"");
            }
        });
        plusJarakB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int jarak;
                try {
                    jarak = Integer.parseInt(jarakB.getText().toString());
                } catch (NumberFormatException ne) {
                    jarak = 0;
                }
                jarak++;
                jarakB.setText(jarak+"");
            }
        });

        // Foul Team A
        minFoulA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int foul;
                try {
                    foul = Integer.parseInt(foulA.getText().toString());
                } catch (NumberFormatException ne) {
                    foul = 0;
                }
                if (--foul < 0) foul = 0;
                foulA.setText(foul+"");
            }
        });
        plusFoulA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int foul;
                try {
                    foul = Integer.parseInt(foulA.getText().toString());
                } catch (NumberFormatException ne) {
                    foul = 0;
                }
                foul++;
                foulA.setText(foul+"");
            }
        });

        // Foul Team B
        minFoulB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int foul;
                try {
                    foul = Integer.parseInt(foulB.getText().toString());
                } catch (NumberFormatException ne) {
                    foul = 0;
                }
                if (--foul < 0) foul = 0;
                foulB.setText(foul+"");
            }
        });
        plusFoulB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int foul;
                try {
                    foul = Integer.parseInt(foulB.getText().toString());
                } catch (NumberFormatException ne) {
                    foul = 0;
                }
                foul++;
                foulB.setText(foul+"");
            }
        });

        // Time Picker setup
        menit1Up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimePicker(menit1, 1, 0, 5);
            }
        });
        menit2Up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimePicker(menit2, 1, 0, 9);
            }
        });
        detik1Up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimePicker(detik1, 1, 0, 5);
            }
        });
        detik2Up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimePicker(detik2, 1, 0, 9);
            }
        });
        milis1Up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimePicker(milis1, 1, 0, 9);
            }
        });
        milis2Up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimePicker(milis2, 1, 0, 9);
            }
        });
        milis3Up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimePicker(milis3, 1, 0, 9);
            }
        });

        menit1Down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimePicker(menit1, -1, 0, 5);
            }
        });
        menit2Down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimePicker(menit2, -1, 0, 9);
            }
        });
        detik1Down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimePicker(detik1, -1, 0, 5);
            }
        });
        detik2Down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimePicker(detik2, -1, 0, 9);
            }
        });
        milis1Down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimePicker(milis1, -1, 0, 9);
            }
        });
        milis2Down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimePicker(milis2, -1, 0, 9);
            }
        });
        milis3Down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateTimePicker(milis3, -1, 0, 9);
            }
        });

        historySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String stopTime = historySpinner.getSelectedItem().toString();
                menit1.setText(stopTime.charAt(0)+"");
                menit2.setText(stopTime.charAt(1)+"");
                detik1.setText(stopTime.charAt(5)+"");
                detik2.setText(stopTime.charAt(6)+"");
                milis1.setText(stopTime.charAt(10)+"");
                milis2.setText(stopTime.charAt(11)+"");
                milis3.setText(stopTime.charAt(12)+"");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Sisa Waktu A
        ubahSisaA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timePicker != null) {
                    timePicker.setButton(AlertDialog.BUTTON_POSITIVE, "OKE",
                            new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String result = "", time;
                            result += menit1.getText(); result += menit2.getText();
                            result += " : "; result += detik1.getText();
                            result += detik2.getText(); result += " : ";
                            result += milis1.getText(); result += milis2.getText();
                            result += milis3.getText();

                            try {
                                SimpleDateFormat parser = new SimpleDateFormat("mm : ss : SSS");
                                long stop = parser.parse(result).getTime(), timer;
                                parser = new SimpleDateFormat("HH : mm : ss : SSS");
                                timer = parser.parse(timerMulai.getText().toString())
                                        .getTime();
                                timer += stop;
                                time = parser.format(new Date(timer));
                            } catch (ParseException pe) {
                                pe.printStackTrace();
                                Toast.makeText(MainActivity.this, "Gagal merubah waktu: "
                                        +pe.toString(), Toast.LENGTH_SHORT).show();
                                return;
                            }

                            time = timerMulai.getText().subSequence(0, 2) + time.substring(2, time.length());
                            String msg, t = getKategori().isEmpty()?""
                                    :getKategori().charAt(0)+"-";
                            msg = t + "Operator_" + t + "Timer:M3_" +
                                    namaA.getText() + "_" + result;
                            if (writer != null) {
                                writer.println(msg); writer.flush();
                            } else {
                                Toast.makeText(MainActivity.this, "Writer kosong, pesan " +
                                        "tidak terkirim !", Toast.LENGTH_LONG).show();
                                return;
                            }
                            sisaA.setText(result); berhentiA.setText(time);
                            timePickerHistoryData.add(0, result);
                            timePickerHistoryAdapter.notifyDataSetChanged();
                            jarakA.setText("50");

                        }
                    });
                    timePicker.setButton(AlertDialog.BUTTON_NEGATIVE, "Clear",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String result = "00 : 00 : 000", time = "00 : 00 : 00 : 000";
                                    String msg, t = getKategori().isEmpty()?""
                                            :getKategori().charAt(0)+"-";
                                    msg = t + "Operator_" + t + "Timer:M3_" +
                                            namaA.getText() + "_" + result;
                                    if (writer != null) {
                                        writer.println(msg); writer.flush();
                                    } else {
                                        Toast.makeText(MainActivity.this, "Writer kosong, pesan " +
                                                "tidak terkirim !", Toast.LENGTH_LONG).show();
                                        return;
                                    } sisaA.setText(result); berhentiA.setText(time);
                                    jarakA.setText("0");
                                }
                            });
                    timePicker.show();
                }
            }
        });
        ubahSisaB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timePicker != null) {
                    timePicker.setButton(AlertDialog.BUTTON_POSITIVE, "OKE",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String result = "", time = "00 : 00 : 00 : 000";
                                    result += menit1.getText(); result += menit2.getText();
                                    result += " : "; result += detik1.getText();
                                    result += detik2.getText(); result += " : ";
                                    result += milis1.getText(); result += milis2.getText();
                                    result += milis3.getText();

                                    try {
                                        SimpleDateFormat parser = new SimpleDateFormat("mm : ss : SSS");
                                        long stop = parser.parse(result).getTime(), timer;
                                        parser = new SimpleDateFormat("HH : mm : ss : SSS");
                                        timer = parser.parse(timerMulai.getText().toString())
                                                .getTime();
                                        timer += stop;
                                        time = parser.format(new Date(timer));
                                    } catch (ParseException pe) {
                                        pe.printStackTrace();
                                        Toast.makeText(MainActivity.this, "Gagal merubah waktu: "
                                                +pe.toString(), Toast.LENGTH_SHORT).show();
                                        return;
                                    }

                                    time = timerMulai.getText().subSequence(0, 2) + time.substring(2, time.length());
                                    String msg, t = getKategori().isEmpty()?""
                                            :getKategori().charAt(0)+"-";
                                    msg = t + "Operator_" + t + "Timer:M3_" +
                                            namaB.getText() + "_" + result;
                                    if (writer != null) {
                                        writer.println(msg); writer.flush();
                                    } else {
                                        Toast.makeText(MainActivity.this, "Writer kosong, pesan " +
                                                "tidak terkirim !", Toast.LENGTH_LONG).show();
                                        return;
                                    }
                                    sisaB.setText(result); berhentiB.setText(time);
                                    timePickerHistoryData.add(0, result);
                                    timePickerHistoryAdapter.notifyDataSetChanged();
                                    jarakB.setText("50");
                                }
                            });
                    timePicker.setButton(AlertDialog.BUTTON_NEGATIVE, "Clear",
                            new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String result = "00 : 00 : 000", time = "00 : 00 : 00 : 000";
                            String msg, t = getKategori().isEmpty()?""
                                    :getKategori().charAt(0)+"-";
                            msg = t + "Operator_" + t + "Timer:M3_" +
                                    namaB.getText() + "_" + result;
                            if (writer != null) {
                                writer.println(msg); writer.flush();
                            } else {
                                Toast.makeText(MainActivity.this, "Writer kosong, pesan " +
                                        "tidak terkirim !", Toast.LENGTH_LONG).show();
                                return;
                            }
                            sisaB.setText(result); berhentiB.setText(time);
                            jarakB.setText("0");
                        }
                    });
                    timePicker.show();
                }
            }
        });

        // Stop Team A
        stopA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long milis = System.currentTimeMillis(), temp = 0;
                Log.d("MainActivity", "StopA Device: "+milis+" - "+"StopA Server: "+(milis + getDeviation()));
                milis += getDeviation();
                SimpleDateFormat formater = new SimpleDateFormat("HH : mm : ss : SSS");
                String stopTime = formater.format(new Date(milis));
                Toast.makeText(MainActivity.this, "Team A Berhenti Pada "+stopTime,
                        Toast.LENGTH_SHORT).show();
                String sisa; temp = milis - getWaktuMulaiTimer();
                sisa = new SimpleDateFormat("mm : ss : SSS").format(new Date(temp));
                Log.d("MainActivity", "timestamp remains timer: "+temp);
                berhentiA.setText(stopTime); sisaA.setText(sisa);
                jarakA.setText("50");

                String msg, t = getKategori().isEmpty()?""
                        :getKategori().charAt(0)+"-";
                msg = t + "Operator_" + t + "Timer:M3_" + getNamaTeamA() + "_" + sisa;
                if (writer != null) {
                    writer.println(msg); writer.flush();
                } else {
                    Toast.makeText(MainActivity.this, "Writer kosong, pesan " +
                            "tidak terkirim !", Toast.LENGTH_LONG).show();
                }
                timePickerHistoryData.add(0, sisa);
                timePickerHistoryAdapter.notifyDataSetChanged();
             }
        });

        // Stop Team B
        stopB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long milis = System.currentTimeMillis(), temp = 0;
                Log.d("MainActivity", "StopB Device: "+milis+" - "+"StopB Server: "+(milis + getDeviation()));
                milis += getDeviation();
                SimpleDateFormat formater = new SimpleDateFormat("HH : mm : ss : SSS");
                String stopTime = formater.format(new Date(milis));
                Toast.makeText(MainActivity.this, "Team B Berhenti Pada "+stopTime,
                        Toast.LENGTH_SHORT).show();
                String sisa; temp = milis - getWaktuMulaiTimer();
                sisa = new SimpleDateFormat("mm : ss : SSS").format(new Date(temp));
                berhentiB.setText(stopTime); sisaB.setText(sisa);
                jarakB.setText("50");

                String msg, t = getKategori().isEmpty()?""
                        :getKategori().charAt(0)+"-";
                msg = t + "Operator_" + t + "Timer:M3_" + getNamaTeamB() + "_" + sisa;
                if (writer != null) {
                    writer.println(msg); writer.flush();
                } else {
                    Toast.makeText(MainActivity.this, "Writer kosong, pesan " +
                            "tidak terkirim !", Toast.LENGTH_LONG).show();
                }
                timePickerHistoryData.add(0, sisa);
                timePickerHistoryAdapter.notifyDataSetChanged();
            }
        });

        // Logo Popo -> Menampilkan menu logout
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (logout != null) logout.show();
            }
        });

        // Mode Timer -> Menampilkan info client
        timerMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (info != null) {
                    info.setMessage("Kategori: "+ getKategori()+
                            "\nNama: "+ getTeam()+
                            "\nSelisih Waktu Server: "+ getDeviation()+
                            " ms\nDurasi Timer: "+durasi+" detik");
                    info.show();
                }
            }
        });

        // Button Refresh -> Membersihkan team dan request data ke operator
        refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (refresh != null) refresh.show();
            }
        });

        // Button Kirim -> Mengirim data ke server
        kirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg, t = getKategori().isEmpty()?"":getKategori().charAt(0)+"-";
                msg = t + "Validator:M2_" + namaA.getText() + "_" + berhentiA.getText() + "_"
                        + sisaA.getText() + "_" + jarakA.getText().toString() + "_" + foulA.getText().toString();
                msg += "_" + namaB.getText() + "_" + berhentiB.getText() + "_" + sisaB.getText()
                        + "_" + jarakB.getText().toString() + "_" + foulB.getText().toString();

                if (writer != null) {
                    writer.println(msg); writer.flush(); // send to server
                } else {
                    Toast.makeText(MainActivity.this, "Writer kosong, pesan tidak terkirim !",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void sendMessages(String msg, String... receiver) {
        String flag = "", t = getKategori().isEmpty()?"":getKategori().charAt(0)+"-";
        for (String rec : receiver) {
            flag += t+rec+"_";
        }
        flag = flag.substring(0, flag.length()-1);
        flag += ":U1_"+getTeam()+"_"+msg; // command
        if (writer != null) {
            writer.println(flag); writer.flush(); // send to server
        } else {
            Toast.makeText(this, "Writer kosong, pesan tidak terkirim !", Toast.LENGTH_LONG).show();
        }
    }

    private void processCommand(String msg) {
        String cmd[] = msg.split("_");
        switch (cmd[0]) {
            case "O1":
                // set team
                sendMessages("Data team berhasil diterima.", "Operator");
                kirim.setEnabled(true);
                if (getTeam().contains("12")) {
                    setNamaTeamA(cmd[1]); setNamaTeamB(cmd[2]);
                    namaA.setText(cmd[1]); namaB.setText(cmd[2]);
                    setTeamAEnable(cmd[5].charAt(0) == 'M');
                    setTeamBEnable(cmd[5].charAt(1) == 'M');
                } else if (getTeam().contains("34")) {
                    setNamaTeamA(cmd[3]); setNamaTeamB(cmd[4]);
                    namaA.setText(cmd[3]); namaB.setText(cmd[4]);
                    setTeamAEnable(cmd[5].charAt(2) == 'M');
                    setTeamBEnable(cmd[5].charAt(3) == 'M');
                }
                Log.d("HandlerMessage", "Done Process command O1");
                break;
            case "O2":
                // clear team
                sendMessages("Berhasil membersihkan team.", "Operator");
                setTeamAEnable(true); setTeamBEnable(true);
                kirim.setEnabled(false); refreshTeam();
                Log.d("HandlerMessage", "Done Process command O2");
                break;
            case "O3":
                // set timer
                TextView tm = (TextView) findViewById(R.id.timer_mode);
                tm.setText(cmd[1]);
                durasi = Integer.parseInt(cmd[2].trim());
                sendMessages("Data timer berhasil diterima", "Operator");
                kirim.setEnabled(true); timerMode.setText(cmd[1]);
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    Log.d("HandlerMessage", "Done Process command O3");
                }
                else Log.d("HandlerMessage", "unfinished Process command O3");
                break;
            case "O4":
                // clear timer
                timerMode.setText("-");
                durasi = 0;
                timerMulai.setText("00 : 00 : 00 : 000");
                timerBerhenti.setText("00 : 00 : 00 : 000");
                sendMessages("Berhasil membersihkan timer", "Operator");
                kirim.setEnabled(false); refreshTeam();
                // setTeamAEnable(true); setTeamBEnable(true);
                Log.d("HandlerMessage", "Done Process command O4");
                break;
            case "O5":
                // start timer
                String pattern = "HH : mm : ss : SSS", mulai, berhenti;
                mulai = new SimpleDateFormat(pattern).format(new Date(Long.parseLong(cmd[1])));
                berhenti = new SimpleDateFormat(pattern).format(new Date(Long.parseLong(cmd[2])));
                timerMulai.setText(mulai); timerBerhenti.setText(berhenti);
                timerMode.setText(cmd[3]); kirim.setEnabled(true);
                setWaktuMulaiTimer(Long.parseLong(cmd[1])); setWaktuBerhentiTimer(Long.parseLong(cmd[2]));
                sendMessages("Berhasil menjalankan timer", "Operator");
                Log.d("HandlerMessage", "Done Process command O5");
                // setTeamAEnable(true); setTeamBEnable(true);
                break;
            case "O6":
                // stop timer
                timerMode.setText("berhenti"); kirim.setEnabled(false);
                sendMessages("Berhasil memberhentikan timer", "Operator");
                // setTeamAEnable(false); setTeamBEnable(false);
                Log.d("HandlerMessage", "Done Process command O6");
                break;
            default:
                Toast.makeText(this, "Gagal memproses pesan: \n"+msg, Toast.LENGTH_SHORT).show();
                Log.d("HandlerMessage", "Done Process command default");
                break;
        }
    }

    private void refreshTeam() {
        setTeamAEnable(true); setTeamBEnable(true);
        timerMulai.setText("00 : 00 : 00 : 000");
        timerBerhenti.setText("00 : 00 : 00 : 000");
        timerMode.setText("-");
        setWaktuMulaiTimer(0); setWaktuBerhentiTimer(0);

        String namaTeamA = getTeam().contains("1")?"Nama Team 1" : "Nama Team 3",
                namaTeamB = getTeam().contains("1")?"Nama Team 2" : "Nama Team 4";
        setNamaTeamA(namaTeamA); setWaktuBerhentiTeamA(0);
        namaA.setText(getNamaTeamA());
        berhentiA.setText("00 : 00 : 00: 000");
        sisaA.setText("00 : 00 : 000");
        jarakA.setText("0"); foulA.setText("0");

        setNamaTeamB(namaTeamB); setWaktuBerhentiTeamB(0);
        namaB.setText(getNamaTeamB());
        berhentiB.setText("00 : 00 : 00: 000");
        sisaB.setText("00 : 00 : 000");
        jarakB.setText("0"); foulB.setText("0");
    }

    private void configMobile() {
        namaA.setText(getNamaTeamA());
        namaA.setBackgroundResource(getDrawableViewTeamA());
        minJarakA.setBackgroundResource(getDrawableViewTeamA());
        plusJarakA.setBackgroundResource(getDrawableViewTeamA());
        minFoulA.setBackgroundResource(getDrawableViewTeamA());
        plusFoulA.setBackgroundResource(getDrawableViewTeamA());
        stopA.setBackgroundResource(getDrawableButtonTeamA());

        namaB.setText(getNamaTeamB());
        namaB.setBackgroundResource(getDrawableViewTeamB());
        minJarakB.setBackgroundResource(getDrawableViewTeamB());
        plusJarakB.setBackgroundResource(getDrawableViewTeamB());
        minFoulB.setBackgroundResource(getDrawableViewTeamB());
        plusFoulB.setBackgroundResource(getDrawableViewTeamB());
        stopB.setBackgroundResource(getDrawableButtonTeamB());

        if (getKategori().equals("Microcontroller")) {
            int black = Color.parseColor("#000000"), white = Color.parseColor("#FFFFFF");
            if (getTeam().contains("12")) {
                namaA.setTextColor(white); namaB.setTextColor(white);
                minJarakA.setTextColor(white); plusJarakA.setTextColor(white);
                minFoulA.setTextColor(white); plusFoulA.setTextColor(white);
                minJarakB.setTextColor(white); plusJarakB.setTextColor(white);
                minFoulB.setTextColor(white); plusFoulB.setTextColor(white);
                stopA.setImageResource(R.mipmap.icon_finish_white);
                stopB.setImageResource(R.mipmap.icon_finish_white);
            } else if (getTeam().contains("34")) {
                namaA.setTextColor(black); namaB.setTextColor(black);
                minJarakA.setTextColor(black); plusJarakA.setTextColor(black);
                minFoulA.setTextColor(black); plusFoulA.setTextColor(black);
                minJarakB.setTextColor(black); plusJarakB.setTextColor(black);
                minFoulB.setTextColor(black); plusFoulB.setTextColor(black);
                stopA.setImageResource(R.mipmap.icon_finish_black);
                stopB.setImageResource(R.mipmap.icon_finish_black);
            }
        } else if (getKategori().equals("Analog")) {
            int black = Color.parseColor("#000000"), white = Color.parseColor("#FFFFFF");
            namaA.setTextColor(white); namaB.setTextColor(black);
            minJarakA.setTextColor(white); plusJarakA.setTextColor(white);
            minFoulA.setTextColor(white); plusFoulA.setTextColor(white);
            minJarakB.setTextColor(black); plusJarakB.setTextColor(black);
            minFoulB.setTextColor(black); plusFoulB.setTextColor(black);
            stopA.setImageResource(R.mipmap.icon_finish_white);
            stopB.setImageResource(R.mipmap.icon_finish_black);
        }

        SimpleDateFormat formater = new SimpleDateFormat("HH : mm : ss : SSS");
        if (getWaktuMulaiTimer() > 0) timerMulai.setText(formater.format(new Date(getWaktuMulaiTimer())));
        else timerMulai.setText("00 : 00 : 00 : 000");
        if (getWaktuBerhentiTimer() > 0)timerBerhenti.setText(formater.format(new Date(getWaktuBerhentiTimer())));
        else timerBerhenti.setText("00 : 00 : 00 : 000");

        if (getWaktuBerhentiTeamA() > 0) {
            berhentiA.setText(formater.format(new Date(getWaktuBerhentiTeamA())));
            long tempA = getWaktuBerhentiTeamA() - getWaktuMulaiTimer();
            sisaA.setText(formater.format(new Date(tempA)));
        } else {
            berhentiA.setText("00 : 00 : 00 : 000");
            sisaA.setText("00 : 00 : 000");
        }
        if (getWaktuBerhentiTeamB() > 0) {
            berhentiB.setText(formater.format(new Date(getWaktuBerhentiTeamB())));
            long tempB = getWaktuBerhentiTeamB() - getWaktuMulaiTimer();
            sisaB.setText(formater.format(new Date(tempB)));
        } else {
            berhentiB.setText("00 : 00 : 00 : 000");
            sisaB.setText("00 : 00 : 000");
        }
    }

    private void setTeamAEnable(boolean status) {
        if (!status) namaA.setText(getNamaTeamA() + " [D]");
        else {
            String newName = namaA.getText().toString();
            if (newName.contains("[D]")) {
                newName = newName.substring(0, newName.length()-4);
            }
            namaA.setText(newName);
        }
        namaA.setEnabled(status);
        stopA.setEnabled(status);
        minJarakA.setEnabled(status);
        minFoulA.setEnabled(status);
        plusJarakA.setEnabled(status);
        plusFoulA.setEnabled(status);
        ubahSisaA.setEnabled(status);
    }

    private void setTeamBEnable(boolean status) {
        if (!status) namaB.setText(getNamaTeamB() + " [D]");
        else {
            String newName = namaB.getText().toString();
            if (newName.contains("[D]")) {
                newName = newName.substring(0, newName.length()-4);
            }
            namaB.setText(newName);
        }
        namaB.setEnabled(status);
        stopB.setEnabled(status);
        minJarakB.setEnabled(status);
        minFoulB.setEnabled(status);
        plusJarakB.setEnabled(status);
        plusFoulB.setEnabled(status);
        ubahSisaA.setEnabled(status);
    }

    private void updateTimePicker(TextView target, int increment, int min, int max) {
        int current = Integer.parseInt(target.getText()+"");
        current += increment;
        if (current < min) current = max;
        else if (current > max) current = min;
        target.setText(current+"");
    }

    private class ClientThread extends Thread {
        private Handler mHandler;

        ClientThread(Handler handler) {
            mHandler = handler;
        }

        @Override
        public void run() {
            running = true;
            if (client != null) mHandler.obtainMessage(1, Toast.LENGTH_SHORT, 1, "Terhubung ke server !")
                    .sendToTarget();
            else running = false;
            while (running) {
                try {
                    String msg = reader.readLine(), cmd[];
                    Log.d("ClientThread", "Message from server: "+msg);

                    cmd = msg.split("_");
                    switch (cmd[0]) {
                        case "U1":
                            mHandler.obtainMessage(1, Toast.LENGTH_LONG, 1, "Pesan dari "+
                                    cmd[1]+": "+cmd[2]).sendToTarget();
                            break;
                        default:
                            mHandler.obtainMessage(2, -1, 1, msg).sendToTarget();
                            break;
                    }
                } catch (Exception ie) {
                    Log.e("ClientThread", "an error occured: "+ie.toString());
                    mHandler.obtainMessage(1, Toast.LENGTH_LONG, 0, "Terputus dari server, " +
                            "error: "+ie.toString()).sendToTarget();
                    running = false;
                }
            }
            try {
                reader.close(); writer.close();
                client.close();
            } catch (Exception e) {
                e.printStackTrace();
                reader = null; writer = null; client = null;
            }
            mHandler.obtainMessage(1, Toast.LENGTH_LONG, 0, "Terputus dari server !").sendToTarget();
            running = false;
            Log.d("ClientThread", "ClientThread stop reading from server");
        }

        void kill() {
            running = false;
            try {
                client.close();
            } catch (Exception e) {
                Log.e("ClientThread", "an error occured while killing thread: "+e.toString());
            }
        }
    }
}
