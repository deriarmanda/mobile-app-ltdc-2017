package um.elektro.wse.ltdc.mobileappltdc2017;

/**
 * Created by DERI on 06/09/2017.
 */

public class SpinnerHistoryModel {
    private String background, textColor, teamName, stopTime;

    public SpinnerHistoryModel(String background, String textColor, String teamName, String stopTime) {
        this.background = background;
        this.textColor = textColor;
        this.teamName = teamName;
        this.stopTime = stopTime;
    }

    public String getBackground() {
        return background;
    }

    public String getTextColor() {
        return textColor;
    }

    public String getTeamName() {
        return teamName;
    }

    public String getStopTime() {
        return stopTime;
    }
}
