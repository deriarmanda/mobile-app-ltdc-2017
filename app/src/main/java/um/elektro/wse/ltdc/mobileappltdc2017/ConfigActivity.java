package um.elektro.wse.ltdc.mobileappltdc2017;

import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.*;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.ipUtil;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.portUtil;

public class ConfigActivity extends AppCompatActivity {

    // View variable
    TextInputEditText ipServer, portServer;
    RadioGroup grupKategori, grupTeam;
    RadioButton checkedKategori, checkedTeam;
    Button connect, syncTime, saveTime;
    EditText msDeviation;
    TextView status, timeLeave, timeServer, timeArrived, timeOffset;

    // Connecting server variable
    Handler loginHandler, syncHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);
        Toolbar toolbar = (Toolbar) findViewById(R.id.config_toolbar);
        setSupportActionBar(toolbar);

        prepareView();
        prepareEventClick();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ipServer != null && ipUtil != null) {
            ipServer.setText(ipUtil);
        }
        if (portServer != null && portUtil != null) {
            portServer.setText(portUtil);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED, null);
        finish();
    }

    private void prepareView() {
        // Handler
        loginHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        Toast.makeText(ConfigActivity.this, msg.obj.toString()+" ke server!",
                                Toast.LENGTH_SHORT).show();
                        status.setText("Status: "+msg.obj.toString());
                        syncTime.setEnabled((msg.arg1 > 0));
                        connect.setEnabled(true);
                        break;
                    default:
                        Toast.makeText(ConfigActivity.this, "Tidak dapat memproses " +
                                "pesan handler: "+msg.what+" - "+msg.obj.toString(),
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };
        syncHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 1:
                        timeLeave.setText(msg.obj.toString()+" ms");
                        break;
                    case 2:
                        timeArrived.setText(msg.obj.toString()+" ms");
                        break;
                    case 3:
                        timeOffset.setText(msg.obj.toString()+" ms");
                        break;
                    case 4:
                        msDeviation.setText(msg.obj.toString());
                        break;
                    case 5:
                        timeServer.setText(msg.obj.toString()+" ms");
                        break;
                    case 6:
                        Toast.makeText(ConfigActivity.this, msg.obj.toString()+" ke server!",
                            Toast.LENGTH_SHORT).show();
                        status.setText("Status: "+msg.obj.toString());
                        syncTime.setEnabled((msg.arg1 > 0));
                        connect.setEnabled(true);
                        break;
                    case 7:
                        saveTime.setEnabled(msg.arg1 > 0);
                        break;
                    default:
                        Toast.makeText(ConfigActivity.this, "Tidak dapat memproses " +
                                "pesan handler: "+msg.what+" - "+msg.obj.toString(),
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        // TextInputEditText
        ipServer = (TextInputEditText) findViewById(R.id.ip_server);
        portServer = (TextInputEditText) findViewById(R.id.port_server);

        // RadioGroup
        grupKategori = (RadioGroup) findViewById(R.id.grup_kategori);
        grupTeam = (RadioGroup) findViewById(R.id.grup_pilih_team);

        // Button
        connect = (Button) findViewById(R.id.connect_btn);
        syncTime = (Button) findViewById(R.id.sinc_btn);
        saveTime = (Button) findViewById(R.id.save_btn);

        // TextView
        status = (TextView) findViewById(R.id.config_status);
        timeLeave = (TextView) findViewById(R.id.time_leave);
        timeServer = (TextView) findViewById(R.id.time_server);
        timeArrived = (TextView) findViewById(R.id.time_arrived);
        timeOffset = (TextView) findViewById(R.id.time_offset);

        // EditText
        msDeviation = (EditText) findViewById(R.id.ms_deviation);
    }

    private void prepareEventClick() {
        // Event at button connect
        if (connect != null) {
            connect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String ip = ipServer.getText().toString(), kategori, team,
                            port = portServer.getText().toString();
                    checkedKategori = (RadioButton) findViewById(grupKategori
                            .getCheckedRadioButtonId());
                    checkedTeam = (RadioButton) findViewById(grupTeam
                            .getCheckedRadioButtonId());
                    kategori = checkedKategori.getText().toString();
                    team = checkedTeam.getText().toString();
                    team = kategori.charAt(0)+(team.contains("1")?"-Mobile12":"-Mobile34");
                    int viewA = -1, viewB = -1, btnA = -1, btnB = -1;
                    if (team.contains("1") && kategori.charAt(0) == 'A') {
                        viewA = R.drawable.bg_selector_red;
                        viewB = R.drawable.bg_selector_yellow;
                        btnA = R.drawable.bg_button_stop_red;
                        btnB = R.drawable.bg_button_stop_yellow;
                        setNamaTeamA("Nama Team 1");
                        setNamaTeamB("Nama Team 2");
                    } else if (team.contains("3") && kategori.charAt(0) == 'A') {
                        viewA = R.drawable.bg_selector_blue;
                        viewB = R.drawable.bg_selector_green;
                        btnA = R.drawable.bg_button_stop_blue;
                        btnB = R.drawable.bg_button_stop_green;
                        setNamaTeamA("Nama Team 3");
                        setNamaTeamB("Nama Team 4");
                    } else if (team.contains("1") && kategori.charAt(0) == 'M') {
                        viewA = R.drawable.bg_selector_red;
                        viewB = R.drawable.bg_selector_blue;
                        btnA = R.drawable.bg_button_stop_red;
                        btnB = R.drawable.bg_button_stop_blue_micro;
                        setNamaTeamA("Nama Team 1");
                        setNamaTeamB("Nama Team 2");
                    } else if (team.contains("3") && kategori.charAt(0) == 'M') {
                        viewA = R.drawable.bg_selector_yellow;
                        viewB = R.drawable.bg_selector_green;
                        btnA = R.drawable.bg_button_stop_yellow_micro;
                        btnB = R.drawable.bg_button_stop_green;
                        setNamaTeamA("Nama Team 3");
                        setNamaTeamB("Nama Team 4");
                    }
                    connect.setEnabled(false);
                    new LoginThread(loginHandler, ip, port, kategori, team).start();

                    setWaktuMulaiTimer(0); setWaktuBerhentiTimer(0);
                    setWaktuBerhentiTeamA(0); setWaktuBerhentiTeamB(0);
                    setDrawableViewTeamA(viewA);setDrawableViewTeamB(viewB);
                    setDrawableButtonTeamA(btnA);setDrawableButtonTeamB(btnB);
                }
            });
        }

        // Event at button Sinkronisasi Waktu
        if (syncTime != null) {
            syncTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (client != null && !client.isClosed()) {
                        new SyncThread(syncHandler).start();
                    } else {
                        Toast.makeText(ConfigActivity.this, "Tidak terhubung ke server!",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }

        // Event at button Simpan
        if (saveTime != null) {
            saveTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    long dev = Long.parseLong(msDeviation.getText().toString());
                    setDeviation(dev);

                    setResult(RESULT_OK, null);
                    finish();
                }
            });
        }
    }

    private class SyncThread extends Thread {
        private Handler mHandler;

        SyncThread(Handler handler) {
            mHandler = handler;
        }

        @Override
        public void run() {
            long departed, arrived, server, offset, deviation;
            String serverResult, departedResult, arrivedResult, offsetResult, deviationResult;

            departed = System.currentTimeMillis();
            try {
                writer.println("M1"); writer.flush();
                serverResult = reader.readLine();
                arrived = System.currentTimeMillis();
            } catch (Exception ie) {
                Log.e("LoginThread", "an error occured: "+ie.toString());
                mHandler.obtainMessage(6, 0, -1, "Tidak Terhubung").sendToTarget();
                return;
            }
            server = Long.parseLong(serverResult);
            offset = (arrived - departed) / 2 ;
            deviation = (server + offset) - arrived;

            departedResult = String.valueOf(departed);
            arrivedResult = String.valueOf(arrived);
            offsetResult = String.valueOf(offset);
            deviationResult = String.valueOf(deviation);

            Log.d("SyncThread", "departed: "+departed+" - "+departedResult);
            Log.d("SyncThread", "arrived: "+arrived+" - "+arrivedResult);
            Log.d("SyncThread", "server: "+server+" - "+serverResult);
            Log.d("SyncThread", "offset: "+offset+" - "+offsetResult);
            Log.d("SyncThread", "deviation: "+deviation+" - "+deviationResult);

            mHandler.obtainMessage(1, departedResult).sendToTarget();
            mHandler.obtainMessage(2, arrivedResult).sendToTarget();
            mHandler.obtainMessage(3, offsetResult).sendToTarget();
            mHandler.obtainMessage(4, deviationResult).sendToTarget();
            mHandler.obtainMessage(5, serverResult).sendToTarget();
            mHandler.obtainMessage(7, 1, -1).sendToTarget();
        }
    }
}
