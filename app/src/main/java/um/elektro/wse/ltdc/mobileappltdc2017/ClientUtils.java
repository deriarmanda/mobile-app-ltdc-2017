package um.elektro.wse.ltdc.mobileappltdc2017;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by DERI on 27/08/2017.
 */

class ClientUtils {
    static Socket client;
    static PrintWriter writer;
    static BufferedReader reader;
    static String ipUtil, portUtil;
    private static String kategori = "-";
    private static String team = "-";
    private static long deviation = 0;

    // Data Pertandingan
    private static String namaTeamA, namaTeamB;
    private static long waktuMulaiTimer, waktuBerhentiTimer;
    private static long waktuBerhentiTeamA, waktuBerhentiTeamB;
    private static int drawableViewTeamA, drawableViewTeamB;
    private static int drawableButtonTeamA, drawableButtonTeamB;

    static String getKategori() {
        return kategori;
    }

    static void setKategori(String kategori) {
        ClientUtils.kategori = kategori;
    }

    static String getTeam() {
        return team;
    }

    static void setTeam(String team) {
        ClientUtils.team = team;
    }

    static long getDeviation() {
        return deviation;
    }

    static void setDeviation(long deviation) {
        ClientUtils.deviation = deviation;
    }

    public static String getNamaTeamA() {
        return namaTeamA;
    }

    public static void setNamaTeamA(String namaTeamA) {
        ClientUtils.namaTeamA = namaTeamA;
    }

    public static String getNamaTeamB() {
        return namaTeamB;
    }

    public static void setNamaTeamB(String namaTeamB) {
        ClientUtils.namaTeamB = namaTeamB;
    }

    public static long getWaktuMulaiTimer() {
        return waktuMulaiTimer;
    }

    public static void setWaktuMulaiTimer(long waktuMulaiTimer) {
        ClientUtils.waktuMulaiTimer = waktuMulaiTimer;
    }

    public static long getWaktuBerhentiTimer() {
        return waktuBerhentiTimer;
    }

    public static void setWaktuBerhentiTimer(long waktuBerhentiTimer) {
        ClientUtils.waktuBerhentiTimer = waktuBerhentiTimer;
    }

    public static long getWaktuBerhentiTeamA() {
        return waktuBerhentiTeamA;
    }

    public static void setWaktuBerhentiTeamA(long waktuBerhentiTeamA) {
        ClientUtils.waktuBerhentiTeamA = waktuBerhentiTeamA;
    }

    public static long getWaktuBerhentiTeamB() {
        return waktuBerhentiTeamB;
    }

    public static void setWaktuBerhentiTeamB(long waktuBerhentiTeamB) {
        ClientUtils.waktuBerhentiTeamB = waktuBerhentiTeamB;
    }

    public static int getDrawableViewTeamA() {
        return drawableViewTeamA;
    }

    public static void setDrawableViewTeamA(int drawableViewTeamA) {
        ClientUtils.drawableViewTeamA = drawableViewTeamA;
    }

    public static int getDrawableViewTeamB() {
        return drawableViewTeamB;
    }

    public static void setDrawableViewTeamB(int drawableViewTeamB) {
        ClientUtils.drawableViewTeamB = drawableViewTeamB;
    }

    public static int getDrawableButtonTeamA() {
        return drawableButtonTeamA;
    }

    public static void setDrawableButtonTeamA(int drawableButtonTeamA) {
        ClientUtils.drawableButtonTeamA = drawableButtonTeamA;
    }

    public static int getDrawableButtonTeamB() {
        return drawableButtonTeamB;
    }

    public static void setDrawableButtonTeamB(int drawableButtonTeamB) {
        ClientUtils.drawableButtonTeamB = drawableButtonTeamB;
    }
}
