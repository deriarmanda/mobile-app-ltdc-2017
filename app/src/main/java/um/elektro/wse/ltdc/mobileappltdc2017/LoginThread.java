package um.elektro.wse.ltdc.mobileappltdc2017;

import android.os.Handler;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.client;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.ipUtil;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.portUtil;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.reader;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.setKategori;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.setTeam;
import static um.elektro.wse.ltdc.mobileappltdc2017.ClientUtils.writer;

/**
 * Created by DERI on 21/09/2017.
 */

public class LoginThread extends Thread {
    private Handler mHandler;
    private String ip, port, kategori, team;

    LoginThread(Handler handler, String... data) {
        mHandler = handler; ip = data[0]; port = data[1];
        kategori = data[2]; team = data[3];
        if (client != null) {
            if (client.isClosed()) return;
            try {
                client.close();
                writer.close();
                reader.close();
            } catch (IOException e) {
                Log.e("LoginThread", "Error pada constructor LoginThread: "+e);
            }
        }
    }

    @Override
    public void run() {
        try {
            setKategori(kategori);
            setTeam(team);
            int portServer = Integer.parseInt(port);
            client = new Socket(ip, portServer);
            writer = new PrintWriter(client.getOutputStream());
            reader = new BufferedReader(new InputStreamReader(
                    client.getInputStream()));

            writer.println(team); writer.flush();

            ipUtil = ip; portUtil = port;
            mHandler.obtainMessage(1, 1, -1, "Terhubung").sendToTarget();
        } catch (Exception ie) {
            Log.e("LoginThread", "an error occured: "+ie.toString());
            try {
                reader.close(); writer.close();
                client.close();
            } catch (Exception e) {
                e.printStackTrace();
                reader = null; writer = null;
                client = null;
            }
            mHandler.obtainMessage(1, 0, -1, "Tidak Terhubung").sendToTarget();
        }
    }
}